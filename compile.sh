#!/bin/bash
#
TARGET="beperf"

HOME=$(pwd)
PDFLATEX="pdflatex --shell-escape -interaction=nonstopmode"
BIBTEX="bibtex"

echo "Pdflatex version: $($PDFLATEX -version | head -1)"
echo "Bibtex version: $($BIBTEX -version | head -1)"

cd $HOME/src

$PDFLATEX $TARGET >> /dev/null
bibtex $TARGET >> /dev/null
$PDFLATEX $TARGET >> /dev/null
$PDFLATEX $TARGET >> /dev/null

cd $HOME

echo "Compiling finished!"
exit
