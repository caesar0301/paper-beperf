# paper-beperf

## Folder structure

- `exports`: The original drawings and experimental figures
for the study.
- `figures`: The figures referred in the paper.
- `src`: source tex and bib files.

## Author

Xiaming Chen - <chenxm35@gmail.com>