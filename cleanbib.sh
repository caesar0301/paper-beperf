#!/bin/bash
# Clean unwanted fields in bib file

input=src/beperf.bib
output=src/beperf-clean.bib

grep -v -E "file|url|urldate|abstract|keywords|address|month|pages|publisher|annote|isbn|issn|doi|language =" $input > $output
